import * as profileModel from "./profileModel.js";
import { generateFile } from "./generateFile.js";
import { validate } from "./isValid.js";
import { deleteFile } from "./deleteFile.js";
import { any } from "./anyErrors.js";
import * as csrf from "./middleware/createToken.js";
import * as registerModel from "./registerModel.js";

import { debug as Debug } from "https://deno.land/x/debug@0.2.0/mod.ts";

const debug = Debug("app:profileController");

export async function profile(ctx) {
  debug("@profile. ctx %O", ctx.request.url);
  const user_id = ctx.session.user.id;
  ctx.response.body = ctx.nunjucks.render("profile.hmtl.njk", {
    products: await profileModel.getAllByUser(ctx.data, user_id),
    authenticated: ctx.state.authenticated,
  });
  ctx.response.status = 200;
  ctx.response.headers["content-type"] = "text/html";
  return ctx;
}

export async function profileEdit(ctx) {
  debug("@profileEdit. ctx %O", ctx.request.url);
  const userId = await ctx.session.user.id;
  const token = csrf.generateToken();
  ctx.session.token = token;
  ctx.response.body = ctx.nunjucks.render("profileEdit.html.njk", {
    user: await profileModel.getUserById(ctx.data, userId),
    authenticated: ctx.state.authenticated,
    csrf: token,
  });
  ctx.response.status = 200;
  ctx.response.headers["content-type"] = "text/html";
  return ctx;
}

export async function submitProfileEdit(ctx) {
  debug("@submitProfileEdit. ctx %O", ctx.request.url);
  const formData = await ctx.request.formData();
  if (ctx.session.token !== formData.get("_csrf")) {
    ctx.response.status = 403;
  } else {
    const userId = await ctx.session.user.id;
    const userOthers = await registerModel.user(ctx.data, userId);
    const userEdit = {
      lastName: formData.get("lastName"),
      firstName: formData.get("firstName"),
      email: formData.get("email").toLowerCase(),
    };

    userEdit.doubleEmail = userOthers
      .map((obj) => obj.email)
      .some((email) => email === userEdit.email);

    const isValid = validate(userEdit);

    if (any(isValid)) {
      ctx.response.body = ctx.nunjucks.render("profileEdit.html.njk", {
        errors: isValid,
        user: userEdit,
        authenticated: ctx.state.authenticated,
        csrf: ctx.session.token,
      });
      ctx.response.status = 200;
      ctx.response.headers["content-type"] = "text/html";
    } else {
      ctx.session.token = undefined;
      profileModel.updateUser(ctx.data, userEdit, userId);

      ctx.response.status = 303;
      ctx.response.headers.location = "/profile";
    }
  }

  return ctx;
}

export async function edit(ctx) {
  debug("@edit. ctx %O", ctx.request.url);
  const id = await ctx.params.id;
  const token = csrf.generateToken();
  ctx.session.token = token;
  ctx.response.body = ctx.nunjucks.render("edit.html.njk", {
    product: await profileModel.detail(ctx.data, id),
    authenticated: ctx.state.authenticated,
    csrf: token,
  });
  ctx.response.status = 200;
  ctx.response.headers["content-type"] = "text/html";
  return ctx;
}

export async function deleteProduct(ctx) {
  debug("@deleteProduct. ctx %O", ctx.request.url);
  const id = await ctx.params.id;
  const filePath = await profileModel.file(ctx.data, id);
  deleteFile(filePath);
  await profileModel.deleteProduct(ctx.data, id);
  ctx.response.status = 303;
  ctx.response.headers.location = "/profile"
  return ctx;
}

export async function submitEdit(ctx) {
  debug("@submitEdit. ctx %O", ctx.request.url);
  const formData = await ctx.request.formData();
  if (ctx.session.token !== formData.get("_csrf")) {
    ctx.response.status = 403;
  } else {
    const id = await ctx.params.id;
    const filePath = await profileModel.file(ctx.data, id);
    const dataEdit = {
      title: formData.get("title"),
      text: formData.get("text"),
      price: formData.get("price"),
      upload: formData.get("file"),
    };

    if (dataEdit.upload.length !== 0) {
      dataEdit.upload = await generateFile(dataEdit.upload);
      if (filePath.length !== 0) {
        deleteFile(filePath);
      }
    } else {
      dataEdit.upload = filePath;
    }

    const isValid = validate(dataEdit);

    if (any(isValid)) {
      ctx.response.body = ctx.nunjucks.render("edit.html.njk", {
        errors: isValid,
        product: dataEdit,
        authenticated: ctx.state.authenticated,
        csrf: ctx.session.token,
      });
      ctx.response.status = 200;
      ctx.response.headers["content-type"] = "text/html";
    } else {
      ctx.session.token = undefined;
      profileModel.update(ctx.data, dataEdit, id);
      ctx.response.status = 303;
      ctx.response.headers.location = "/profile";
    }
  }
  return ctx;
}
