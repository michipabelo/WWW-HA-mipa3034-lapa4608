import { debug as Debug } from "https://deno.land/x/debug@0.2.0/mod.ts";
import { deleteProduct } from "./profileModel.js";

const debug = Debug("app:checkoutModel");



export async function getProductId(data, ID) {
    debug("@getProductId");
    const sql = "SELECT id FROM checkout where buyer_id=?";
    const result = await data.queryEntries(sql, [ID]);
    return result;
}

export function add(data, product, ID) {
    debug("@add");
    const sql = `INSERT INTO checkout
  (id,buyer_id) VALUES (?,?)`;
    data.query(sql, [
        product.id,
        ID,
    ]);
    return data.lastInsertRowId;
}

export async function getProductById(data, ID) {
    debug("@getProductById");
    const sql = "SELECT * FROM product where id=?";
    const result = await data.queryEntries(sql, [ID]);
    return result[0];
}

export async function getAllProductsById(data, ID) {
    debug("@getAllProductsById");
    const product_id = ID.map(e => {
        return e.id;
    });

    let qm = "";
    for (let id in product_id) {
        qm += "?,";
    }
    qm = qm.slice(0, -1);

    const sql = `SELECT * FROM product where id IN (${qm})`;
    const result = await data.queryEntries(sql, product_id);
    return result
}


export async function buy(data, products, id) {
    debug("@buy");
    const sql = "INSERT INTO buyed (id,title,text,price,buyer_id) VALUES(?,?,?,?,?)"
    for (let i = 0; i < products.length; i++) {
        const element = products[i];
        await data.query(sql, [element.id, element.title, element.text, element.price, id]);
        await deleteProduct(data, element.id);
    }




}




export function deleteCheckout(data, ID) {
    debug("@deleteCheckout");
    const sql = `DELETE from checkout where id =?`;
    const result = data.queryEntries(sql, [ID]);
    return result;
}
