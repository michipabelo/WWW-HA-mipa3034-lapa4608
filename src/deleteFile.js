import { path } from "https://deno.land/x/nunjucks@3.2.3/src/deps.js";

export const deleteFile = async (filePath) => {
  const fullPath = path.join(Deno.cwd(), "public", filePath);

  await Deno.remove(fullPath);
};
