import * as checkoutModel from "./checkoutModel.js";
import { debug as Debug } from "https://deno.land/x/debug@0.2.0/mod.ts";

const debug = Debug("app:checkoutController");



export async function checkout(ctx) {
    debug("@checkout. ctx %O", ctx.request.url);
    const buyer_id = ctx.session.user.id;
    const product_id = await checkoutModel.getProductId(ctx.data, buyer_id);

    const products = await checkoutModel.getAllProductsById(ctx.data, product_id);
    const product_prices = products.map((product) => product.price).reduce((a, b) => a + b, 0)
    ctx.response.body = ctx.nunjucks.render("warenkorb.html.njk", {
        products: products,
        sum: product_prices,
        buyer_id: buyer_id,
        authenticated: ctx.state.authenticated,
    });
    ctx.response.status = 200;
    ctx.response.headers["content-type"] = "text/html";

    return ctx;
}

export async function add(ctx) {
    debug("@add. ctx %O", ctx.request.url);
    const product_id = await ctx.params.id;
    const product = await checkoutModel.getProductById(ctx.data, product_id);
    const buyer_id = ctx.session.user.id;
    await checkoutModel.add(ctx.data, product, buyer_id);
    ctx.response.status = 303;
    ctx.response.headers.location = `/detail/${product_id}`;

    return ctx;
}


export async function deleteCheckout(ctx) {
    debug("@deleteCheckout. ctx %O", ctx.request.url);
    const product_id = ctx.params.id
    await checkoutModel.deleteCheckout(ctx.data, product_id);
    ctx.response.status = 303;
    ctx.response.headers.location = "/checkout"
    return ctx;
}

export async function buy(ctx) {
    debug("@deleteCheckout. ctx %O", ctx.request.url);
    const buyer_id = ctx.session.user.id;
    const product_id = await checkoutModel.getProductId(ctx.data, buyer_id);
    const products = await checkoutModel.getAllProductsById(ctx.data, product_id);

    await checkoutModel.buy(ctx.data, products, buyer_id);
    ctx.response.status = 303;
    ctx.response.headers.location = "/checkout"
    return ctx;
}