import { debug as Debug } from "https://deno.land/x/debug@0.2.0/mod.ts";

const debug = Debug("app:model");

/**
 * * Loads all notes.
 * @param {Object[]} data – All notes.
 * @returns {Object[]} – All notes.
 */

export async function getAll(data) {
  debug("@getAll");
  const sql = "SELECT * FROM product";
  const result = await data.queryEntries(sql);
  return result;
}

/**
 * Get one note.
 * @param {Object[]} data – All notes.
 * @param {number} id – Id of a note
 * @returns {Object|undefined} – The note or undefined.
 */
export async function detail(data, ID) {
  debug("@detail");
  const sql = "SELECT * FROM product WHERE ID=? ";
  const result = await data.queryEntries(sql, [ID]);
  return result[0];
}

/**
 * Add a note.
 * @param {Object[]} data – All notes.
 * @param {Object} formData – Note to add.
 */

export function add(data, product) {
  debug("@add");
  const sql = `INSERT INTO product
  (title,text,price,upload,user_id) VALUES (?,?,?,?,?)`;
  data.query(sql, [
    product.title,
    product.text,
    product.price,
    product.upload,
    product.user_id,
  ]);
  return data.lastInsertRowId;
}
