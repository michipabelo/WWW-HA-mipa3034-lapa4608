export const any = (errors) =>
  Boolean(Object.values(errors).find((item) => item));
