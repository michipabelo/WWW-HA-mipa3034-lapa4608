import * as model from "./model.js";
import { debug as Debug } from "https://deno.land/x/debug@0.2.0/mod.ts";

const debug = Debug("app:controller");

export function error404(ctx) {
  debug("@error404. ctx %O", ctx.request.url);
  ctx.response.body = ctx.nunjucks.render("error404.html.njk", {});
  ctx.response.status = 404;
  ctx.response.headers["content-type"] = "text/html";
  return ctx;
}
export function error403(ctx) {
  debug("@error403. ctx %O", ctx.request.url);
  ctx.response.body = ctx.nunjucks.render("error403.html.njk", {});
  ctx.response.status = 403;
  ctx.response.headers["content-type"] = "text/html";
  return ctx;
}
export function datenschutz(ctx) {
  debug("@datenschutz. ctx %O", ctx.request.url);
  ctx.response.body = ctx.nunjucks.render("datenschutz.html.njk", {
    authenticated: ctx.state.authenticated,
  });
  ctx.response.status = 200;
  ctx.response.headers["content-type"] = "text/html";
  return ctx;
}

export function impressum(ctx) {
  debug("@impressum. ctx %O", ctx.request.url);
  ctx.response.body = ctx.nunjucks.render("impressum.html.njk", {
    authenticated: ctx.state.authenticated,
  });
  ctx.response.status = 200;
  ctx.response.headers["content-type"] = "text/html";
  return ctx;
}

export function kollophon(ctx) {
  debug("@kollophon. ctx %O", ctx.request.url);
  ctx.response.body = ctx.nunjucks.render("kollophon.html.njk", {
    authenticated: ctx.state.authenticated,
  });
  ctx.response.status = 200;
  ctx.response.headers["content-type"] = "text/html";
  return ctx;
}

export async function detail(ctx) {
  debug("@detail. ctx %O", ctx.request.url);
  const imgId = await ctx.params.id;
  ctx.response.body = ctx.nunjucks.render("detail.html.njk", {
    product: await model.detail(ctx.data, imgId),
    authenticated: ctx.state.authenticated,
  });
  ctx.response.status = 200;
  ctx.response.headers["content-type"] = "text/html";
  return ctx;
}

export async function index(ctx) {
  debug("@index. ctx %O", ctx.request.url);
  ctx.response.body = ctx.nunjucks.render("index.html.njk", {
    products: await model.getAll(ctx.data),
    authenticated: ctx.state.authenticated,
    user: ctx.session.user,
  });

  ctx.response.status = 200;
  ctx.response.headers["content-type"] = "text/html";
  return ctx;
}

export function indexDoku(ctx) {
  debug("@indexDoku. ctx %O", ctx.request.url);
  ctx.response.body = ctx.nunjucks.render("indexDoku.html.njk", {
    authenticated: ctx.state.authenticated,
  });
  ctx.response.status = 200;
  ctx.response.headers["content-type"] = "text/html";
  return ctx;
}

export function timeline(ctx) {
  debug("@timeline. ctx %O", ctx.request.url);
  ctx.response.body = ctx.nunjucks.render("timeline.html.njk", {
    authenticated: ctx.state.authenticated,
  });
  ctx.response.status = 200;
  ctx.response.headers["content-type"] = "text/html";
  return ctx;
}
