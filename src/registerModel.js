import { hash } from "https://deno.land/x/bcrypt@v0.4.1/mod.ts";

import { debug as Debug } from "https://deno.land/x/debug@0.2.0/mod.ts";

const debug = Debug("app:registerModel");

export async function hashSyncPw(userPassword) {
  return await hash(userPassword);
}

/**
 * * Loads all notes.
 * @param {Object[]} data – All notes.
 * @returns {Object[]} – All notes.
 */

export async function user(data, ID) {
  debug("@user");
  const sql = "SELECT email FROM user where not id=?";
  const result = await data.queryEntries(sql, [ID]);
  return result;
}

/**
 * Add a note.
 * @param {Object[]} data – All notes.
 * @param {Object} formData – Note to add.
 */

export function add(data, user) {
  debug("@add");
  const sql = `INSERT INTO user
  (lastname,firstname,email,password) VALUES (?,?,?,?)`;
  data.query(sql, [user.lastName, user.firstName, user.email, user.password]);
  return data.lastInsertRowId;
}
