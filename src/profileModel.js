import { debug as Debug } from "https://deno.land/x/debug@0.2.0/mod.ts";

const debug = Debug("app:profileModel");


export async function getAllByUser(data, userId) {
  debug("@getAllByUser");
  const sql = "SELECT * FROM product Where user_id =? ";
  const result = await data.queryEntries(sql, [userId]);
  return result;
}

export async function detail(data, ID) {
  debug("@detail");
  const sql = "SELECT * FROM product WHERE ID=? ";
  const result = await data.queryEntries(sql, [ID]);
  return result[0];
}

/**
 * Update a note.
 * @param {Object[]} data – All notes.
 * @param {Object} formData – Notedata for update to.
 * @param {number} id – Id of a note
 */
export const update = (data, product, ID) => {
  debug("@update");
  const sql = `UPDATE product 
  SET title = ?, text = ?, price = ?, upload = ?
  WHERE id=? `;
  data.query(sql, [
    product.title,
    product.text,
    product.price,
    product.upload,
    ID,
  ]);
  return data.queryEntries(sql);
};

export const file = (data, ID) => {
  debug("@file");
  const sql = `SELECT upload from product where id = ?`;
  const result = data.queryEntries(sql, [ID]);
  return result[0].upload;
};

export const deleteProduct = (data, ID) => {
  debug("@deletProduct");
  const sql = `DELETE from product where id =?`;
  const result = data.queryEntries(sql, [ID]);
  return result;
};

export const getUserById = (data, ID) => {
  debug("@getUserById");
  const sql = `SELECT * from user where id = ?`;
  const result = data.queryEntries(sql, [ID]);
  return result[0];
};


export const updateUser = (data, user, ID) => {
  debug("@updateUser");
  const sql = `UPDATE user 
  SET firstname = ?, lastname = ?, email = ?
  WHERE id=? `;
  data.query(sql, [
    user.firstName,
    user.lastName,
    user.email,
    ID,
  ]);

  return data.queryEntries(sql);
};
