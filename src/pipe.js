export const pipeAsync = (...funcs) => (argarg) =>
  funcs.reduce(async (state, func) => func(await state), argarg);
