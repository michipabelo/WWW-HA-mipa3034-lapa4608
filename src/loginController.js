import * as loginModel from "./loginModel.js";
import { debug as Debug } from "https://deno.land/x/debug@0.2.0/mod.ts";
import { validate } from "./isValid.js";
import { any } from "./anyErrors.js";
import * as csrf from "./middleware/createToken.js";
const debug = Debug("app:loginController");

export function add(ctx) {
  debug("@add. ctx %O", ctx.request.url);
  const token = csrf.generateToken();
  ctx.session.token = token;
  ctx.response.body = ctx.nunjucks.render("loginForm.html.njk", {
    csrf: token,
  });
  ctx.response.status = 200;
  ctx.response.headers["content-type"] = "text/html";
  return ctx;
}

export async function submitAdd(ctx) {
  debug("@submitAdd. ctx %O", ctx.request.url);
  const formData = await ctx.request.formData();
  if (ctx.session.token !== formData.get("_csrf")) {
    ctx.response.status = 403;
  } else {
    const user = {
      username: formData.get("username").toLowerCase(),
      password: formData.get("password"),
    };
    const userData = await loginModel.getByUsername(ctx.data, user.username);

    const isValid = validate(user);

    if (any(isValid)) {
      ctx.response.body = ctx.nunjucks.render("loginForm.html.njk", {
        errors: isValid,
        user: user,
        csrf: ctx.session.token,
      });
      ctx.response.status = 200;
      ctx.response.headers["content-type"] = "text/html";
    } else if (userData === undefined) {
      ctx.response.body = ctx.nunjucks.render("loginForm.html.njk", {
        errors: { error: "Keine Übereinstimmung" },
        user: user,
        csrf: ctx.session.token,
      });
      ctx.response.status = 200;
      ctx.response.headers["content-type"] = "text/html";
    } else if (
      (await loginModel.hashPassword(user.password, userData.password)) === true
    ) {
      userData.password = undefined;
      ctx.session.token = undefined;
      ctx.session.user = userData;
      ctx.session.flash = `Du bist als ${userData.firstname} eingeloggt.`;
      ctx.response.status = 303;
      ctx.response.headers.location = "/";
    } else {
      ctx.response.body = ctx.nunjucks.render("loginForm.html.njk", {
        errors: {
          error:
            "Diese Kombination aus Nutzername und Passwort ist nicht gültig",
        },
        user: user,
        csrf: ctx.session.token,
      });
      ctx.response.status = 200;
      ctx.response.headers["content-type"] = "text/html";
    }

  }
  return ctx;
}

export function logout(ctx) {
  debug("@logout. ctx %O", ctx.request.url);
  ctx.session.user = undefined;
  ctx.session.flash = `Du hast dich ausgeloggt.`;
  ctx.response.status = 302;
  ctx.response.headers.location = "/";

  return ctx;
}
