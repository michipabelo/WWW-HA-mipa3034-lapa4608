import * as model from "./model.js";
import { generateFile } from "./generateFile.js";
import { debug as Debug } from "https://deno.land/x/debug@0.2.0/mod.ts";
import { validate } from "./isValid.js";
import * as csrf from "./middleware/createToken.js";
const debug = Debug("app:formController");

export function add(ctx) {
  debug("@form. ctx %O", ctx.request.url);
  const token = csrf.generateToken();
  ctx.session.token = token;
  ctx.response.body = ctx.nunjucks.render("anzeige.html.njk", {
    authenticated: ctx.state.authenticated,
    csrf: token,
  });
  ctx.response.status = 200;
  ctx.response.headers["content-type"] = "text/html";
  return ctx;
}

export async function submitAdd(ctx) {
  const user_id = ctx.session.user.id;
  const formData = await ctx.request.formData();
  if (ctx.session.token !== formData.get("_csrf")) {
    ctx.response.status = 403;
  } else {
    const data = {
      title: formData.get("title"),
      text: formData.get("text"),
      price: formData.get("price"),
      upload: formData.get("file"),
      user_id: user_id,
    };

    const isValid = validate(data);

    if (Object.keys(isValid).length !== 0) {
      ctx.response.body = ctx.nunjucks.render("anzeige.html.njk", {
        errors: isValid,
        product: data,
        authenticated: ctx.state.authenticated,
        csrf: ctx.session.token,
      });
      ctx.response.status = 200;
      ctx.response.headers["content-type"] = "text/html";
    } else {
      data.upload = await generateFile(data.upload);
      ctx.session.token = undefined;
      model.add(ctx.data, data);
      ctx.response.status = 303;
      ctx.response.headers.location = "http://localhost:9000/";
    }
  }

  return ctx;
}
