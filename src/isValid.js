export const validate = (data) => {
  const valid = {};

  if (data.title != null) {
    if (data.title.length === 0) {
      valid.title = "Produkttitel eingeben";
    }
  }
  if (data.text != null) {
    if (data.text.length <= 10) {
      valid.text = "Der eingegebene Text ist zu kurz";
    }
  }
  if (data.price != null) {
    if (data.price.length === 0) {
      valid.price = "Preis eingegeben";
    }
  }
  if (data.upload != null) {
    if (data.upload.length === 0) {
      valid.upload = "kein Bild ausgewählt";
    }
  }
  if (data.lastName != null) {
    if (data.lastName.length === 0) {

      valid.lastName = "Nachname eingeben";
    }
  }
  if (data.firstName != null) {
    if (data.firstName.length === 0) {

      valid.firstName = "Vorname eingeben";
    }
  }
  if (data.email != null) {
    if (data.email.length === 0) {

      valid.email = "E-Mail eingeben";
    }
    if (data.doubleEmail === true) {
      valid.email = "Diese Email ist schon vorhanden";
    }
  }
  if (data.password != null) {
    if (data.password.length <= 8 && data.password.length > 0) {

      valid.password = "Password ist zu kurz";
    } else if (data.password.length === 0) {
      valid.password = "Passwort eingeben";
    }
  }
  if (data.password != null && data.passwordCheck != null) {
    if (data.password.length !== data.passwordCheck.length) {

      valid.password = "Die Passwörter stimmen nicht überein";
    }
  }
  if (data.username != null) {
    if (data.username.length === 0) {

      valid.username = "E-Mail eingeben";
    }
  }

  return valid;
};
