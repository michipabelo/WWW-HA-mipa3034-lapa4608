export const isAuthenticated = (ctx) => {
  if (!ctx.state.user) {
    ctx.response.status = 403; // Forbidden 
    return ctx;
  }

  ctx.state.authenticated = true;
  return ctx;
};
