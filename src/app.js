import { serveStaticFile } from "./middleware/staticFile.js";
import { fallback } from "./middleware/errorStatus.js";
import { currentUser } from "./middleware/currentUser.js";
import { pipeAsync } from "./pipe.js";
import { router } from "./middleware/router.js";
import { routerLogin } from "./middleware/routerLogin.js";
import { routerPost } from "./middleware/routerPost.js";
import * as cookies from "./middleware/cookies.js";
import { createContext } from "./middleware/createContext.js";

cookies.createId();

const createResponse = (result) => {
  return new Response(result.response.body, {
    status: result.response.status,
    headers: result.response.headers,
  });
};

export const handleRequest = async (request) => {
  let ctx = createContext(request);

  cookies.getCookies(ctx);

  ctx = await pipeAsync(
    serveStaticFile("public"),
    currentUser,
    router,
    routerLogin,
    routerPost,
    fallback,
  )(ctx);

  cookies.setCookies(ctx);

  return createResponse(ctx);
};
