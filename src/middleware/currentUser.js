import { getUserById } from "../profileModel.js";


export const currentUser = async (ctx) => {
  let user;

  if (Object.keys(ctx.session).length !== 0) {
    user = await getUserById(ctx.data, ctx.session.user?.id);
  }

  ctx.state.user = user;

  return ctx;
};
