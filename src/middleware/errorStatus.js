import * as controller from "../controller.js";


export const fallback = async (ctx) => {
  ctx.response.status = ctx.response.status ?? 404;
  if (!ctx.response.body && ctx.response.status == 404) {
    ctx = await controller.error404(ctx);
  }

  ctx.response.status = ctx.response.status ?? 403;
  if (!ctx.response.body && ctx.response.status == 403) {
    ctx = await controller.error403(ctx);
  }

  return ctx;
};
