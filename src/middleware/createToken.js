import {
    encode as base64Encode
} from "https://deno.land/std@0.171.0/encoding/base64.ts";

export const generateToken = () => {
    const array = new Uint32Array(64);
    crypto.getRandomValues(array);
    return base64Encode(array);
}

