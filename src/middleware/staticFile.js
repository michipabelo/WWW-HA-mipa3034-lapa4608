import * as path from "https://deno.land/std@0.167.0/path/posix.ts";
import * as mediaTypes from "https://deno.land/std@0.151.0/media_types/mod.ts";

/* StaticFile Methode aufruf von statischen Dateien*/
export const serveStaticFile = (base) => async (ctx) => {
  let file;
  const fullPath = path.join(base, ctx.url.pathname);

  if (fullPath.indexOf(base) !== 0 || fullPath.indexOf("\0") !== -1) {
    ctx.response.status = 403;
    return ctx;
  }
  try {
    file = await Deno.open(fullPath, { read: true });
  } catch (_error) {
    return ctx;
  }
  const { ext } = path.parse(ctx.url.pathname);
  const contentType = mediaTypes.contentType(ext);
  if (contentType) {
    ctx.response.body = file.readable;
    ctx.response.headers["content-type"] = contentType;
    ctx.response.status = 200;
  } else {
    Deno.close(file.rid);
  }

  return ctx;
};
