import * as controller from "../controller.js";
import * as registerController from "../registerController.js";
import * as loginController from "../loginController.js";

import { isAuthenticated } from "../isAuthenticated.js";

export const router = async (ctx) => {
  const url = new URL(ctx.request.url);
  const pattern = [{
    pattern: new URLPattern({ pathname: "/" }),
    controller: controller.index,
  }, {
    pattern: new URLPattern({ pathname: "/datenschutz" }),
    controller: controller.datenschutz,
  }, {
    pattern: new URLPattern({ pathname: "/impressum" }),
    controller: controller.impressum,
  }, {
    pattern: new URLPattern({ pathname: "/detail/:id" }),
    controller: controller.detail,
  }, {
    pattern: new URLPattern({ pathname: "/register" }),
    controller: registerController.add,
  }, {
    pattern: new URLPattern({ pathname: "/login" }),
    controller: loginController.add,
  }, {
    pattern: new URLPattern({ pathname: "/kollophon" }),
    controller: controller.kollophon,
  }, {
    pattern: new URLPattern({ pathname: "/indexDoku" }),
    controller: controller.indexDoku,
  }, {
    pattern: new URLPattern({ pathname: "/timeline" }),
    controller: controller.timeline,
  }];

  if (pattern.some((element) => element.pattern.test(url))) {
    const routes = pattern.find((e) => e.pattern.test(url));
    const match = routes.pattern.exec(ctx.request.url);
    ctx.params.id = match.pathname.groups.id;
    ctx = isAuthenticated(ctx);
    if (ctx.request.method === "GET") {
      await routes.controller(ctx);
    }
  }
  return ctx;
};
