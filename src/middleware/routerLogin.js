import * as profileController from "../profileController.js";
import * as anzeigeController from "../anzeigeController.js";
import * as loginController from "../loginController.js";
import * as checkoutController from "../checkoutController.js";
import { isAuthenticated } from "../isAuthenticated.js";

export const routerLogin = async (ctx) => {
  const url = new URL(ctx.request.url);
  const pattern = [{
    pattern: new URLPattern({ pathname: "/anzeige" }),
    controller: anzeigeController.add,
  }, {
    pattern: new URLPattern({ pathname: "/profile" }),
    controller: profileController.profile,
  }, {
    pattern: new URLPattern({ pathname: "/profile/:id/delete" }),
    controller: profileController.deleteProduct,
  }, {
    pattern: new URLPattern({ pathname: "/profileEdit" }),
    controller: profileController.profileEdit,
  }, {
    pattern: new URLPattern({ pathname: "/logout" }),
    controller: loginController.logout,
  }, {
    pattern: new URLPattern({ pathname: "/profile/:id/edit" }),
    controller: profileController.edit,
  }, {
    pattern: new URLPattern({ pathname: "/add/:id" }),
    controller: checkoutController.add,
  }, {
    pattern: new URLPattern({ pathname: "/checkout" }),
    controller: checkoutController.checkout,
  },
  {
    pattern: new URLPattern({ pathname: "/checkout/:id/delete" }),
    controller: checkoutController.deleteCheckout,
  },
  {
    pattern: new URLPattern({ pathname: "/checkout/buy" }),
    controller: checkoutController.buy,
  }
  ];

  if (pattern.some((element) => element.pattern.test(url))) {
    const routes = pattern.find((e) => e.pattern.test(url));
    const match = routes.pattern.exec(ctx.request.url);
    ctx.params.id = match.pathname.groups.id;
    ctx = isAuthenticated(ctx);
    if (ctx.response.status) {
      return ctx;
    }
    if (ctx.request.method === "GET") {
      await routes.controller(ctx);
    }
  }
  return ctx;
};
