import * as sqlite from "https://deno.land/x/sqlite@v3.7.0/mod.ts";
import nunjucks from "https://deno.land/x/nunjucks@3.2.3/mod.js";
import * as cookies from "./cookies.js";

/* Einlesen der Datenbank */
export const db = new sqlite.DB("./src/shop_db.db", { mode: "write" });

nunjucks.configure("templates", { autoescape: true, noCache: true });

const sessionStore = cookies.createSessionStore();

export const createContext = (request) => {
  const ctx = {
    data: db,
    nunjucks: nunjucks,
    request: request,
    url: new URL(request.url),
    params: {
      id: undefined,
    },
    response: {
      body: undefined,
      status: undefined,
      headers: {},
    },
    state: {},
    cookies: {},
    session: {
      flash: undefined,
      user: undefined,
    },
    sessionStore: sessionStore,
  };
  return ctx;
};
