import {
  CookieMap,
  mergeHeaders,
} from "https://deno.land/std@0.171.0/http/mod.ts";
import { encode as base64Encode } from "https://deno.land/std@0.171.0/encoding/base64.ts";

const SESSION_KEY = "SECRET";
const MAX_AGE = 60 * 60 * 1000;

export const createId = () => {
  const array = new Uint32Array(64);
  crypto.getRandomValues(array);
  return base64Encode(array);
};

export const createSessionStore = () => {
  const sessionStore = new Map();
  return {
    get(key) {
      const data = sessionStore.get(key);
      if (!data) {
        return;
      }
      return data.maxAge < Date.now() ? this.destroy(key) : data.session;
    },
    set(key, session, maxAge) {
      sessionStore.set(key, {
        session,
        maxAge: Date.now() + maxAge,
      });
    },
    destroy(key) {
      sessionStore.delete(key);
    },
  };
};

export const getCookies = (ctx) => {
  ctx.start = Date.now();

  //Get Cookie
  ctx.cookies = new CookieMap(ctx.request);

  //Get Session
  ctx.sessionId = ctx.cookies.get(SESSION_KEY);
  ctx.session = ctx.sessionStore.get(ctx.sessionId, MAX_AGE) ?? {};
};

export const setCookies = (ctx) => {
  //Set Session
  if (Object.values(ctx.session) == 0) {
    ctx.sessionStore.destroy(ctx.sessionId);
    ctx.cookies.delete(SESSION_KEY);
  } else {
    ctx.sessionId = ctx.sessionId ?? createId();
    ctx.sessionStore.set(ctx.sessionId, ctx.session, MAX_AGE);
    const maxAge = new Date(Date.now() + MAX_AGE);
    ctx.cookies.set(SESSION_KEY, ctx.sessionId, {
      expires: maxAge,
      httpOnly: true,
      overwrite: true,
    });
  }
  //Set Cookie
  const allHeaders = mergeHeaders(ctx.response.headers, ctx.cookies);
  ctx.response.headers["set-cookie"] = allHeaders.get("set-cookie");

  //Timeing
  if (ctx.start) {
    const ms = Date.now() - ctx.start;
    ctx.response.headers["X-Respone-Time"] = `${ms}ms`;
  }
};
