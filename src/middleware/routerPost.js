import * as profileController from "../profileController.js";
import * as anzeigeController from "../anzeigeController.js";
import * as registerController from "../registerController.js";
import * as loginController from "../loginController.js";
import { isAuthenticated } from "../isAuthenticated.js";

export const routerPost = async (ctx) => {

  const url = new URL(ctx.request.url);
  const pattern = [{
    pattern: new URLPattern({ pathname: "/profileEdit" }),
    controller: profileController.submitProfileEdit,
  }, {
    pattern: new URLPattern({ pathname: "/anzeige" }),
    controller: anzeigeController.submitAdd,
  }, {
    pattern: new URLPattern({ pathname: "/profile/:id/edit" }),
    controller: profileController.submitEdit,
  }, {
    pattern: new URLPattern({ pathname: "/register" }),
    controller: registerController.submitAdd,
  }, {
    pattern: new URLPattern({ pathname: "/login" }),
    controller: loginController.submitAdd,
  }];

  if (pattern.some((element) => element.pattern.test(url))) {
    const routes = pattern.find((e) => e.pattern.test(url));
    const match = routes.pattern.exec(ctx.request.url);
    ctx.params.id = match.pathname.groups.id;
    ctx = isAuthenticated(ctx);
    if (ctx.request.method === "POST") {
      await routes.controller(ctx);
    }
  }
  return ctx;
};
