import * as registerModel from "./registerModel.js";
import { debug as Debug } from "https://deno.land/x/debug@0.2.0/mod.ts";
import { validate } from "./isValid.js";
import { any } from "./anyErrors.js";
import * as csrf from "./middleware/createToken.js";
const debug = Debug("app:registerController");

export function add(ctx) {
  debug("@add. ctx %O", ctx.request.url);
  const token = csrf.generateToken();
  ctx.session.token = token;
  ctx.response.body = ctx.nunjucks.render("registerForm.html.njk", {
    csrf: token,
  });
  ctx.response.status = 200;
  ctx.response.headers["content-type"] = "text/html";
  return ctx;
}

export async function submitAdd(ctx) {
  debug("@submitAdd. ctx %O", ctx.request.url);
  const formData = await ctx.request.formData();
  if (ctx.session.token !== formData.get("_csrf")) {
    ctx.response.status = 403;
  } else {
    const userEmail = await registerModel.user(ctx.data);
    const user = {
      lastName: formData.get("lastName"),
      firstName: formData.get("firstName"),
      email: formData.get("email").toLowerCase(),
      password: formData.get("password"),
      passwordCheck: formData.get("passwordCheck"),
    };

    user.doubleEmail = userEmail
      .map((obj) => obj.email)
      .some((email) => email === user.email);

    const isValid = validate(user);

    if (any(isValid)) {
      ctx.response.body = ctx.nunjucks.render("registerForm.html.njk", {
        errors: isValid,
        user: user,
        csrf: ctx.session.token,
      });
      ctx.response.status = 200;
      ctx.response.headers["content-type"] = "text/html";
    } else {
      ctx.session.token = undefined;
      user.password = await registerModel.hashSyncPw(user.password);
      registerModel.add(ctx.data, user);
      ctx.response.status = 303;
      ctx.response.headers.location = "/login";
    }
  }

  return ctx;
}
