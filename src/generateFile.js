import * as mediaTypes from "https://deno.land/std@0.167.0/media_types/mod.ts";
import { path } from "https://deno.land/x/nunjucks@3.2.3/src/deps.js";

export const generateFile = async (file) => {
  const fileName = path.join(
    "upload",
    crypto.randomUUID() + "." + mediaTypes.extension(file.type),
  );

  const destFile = await Deno.open(path.join(Deno.cwd(), "public", fileName), {
    create: true,
    write: true,
    truncate: true,
  });
  await file.stream().pipeTo(destFile.writable);
  /* Deno.close(file); */
  return fileName;
};
