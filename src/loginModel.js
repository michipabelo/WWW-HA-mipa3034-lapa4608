import { debug as Debug } from "https://deno.land/x/debug@0.2.0/mod.ts";
import { compare } from "https://deno.land/x/bcrypt@v0.4.1/mod.ts";


const debug = Debug("app:loginModel");

/**
 * * Loads all notes.
 * @param {Object[]} data – All notes.
 * @returns {Object[]} – All notes.
 */

export async function hashPassword(inputPassword, userPassword) {
  debug("@hashPassword");
  return await compare(inputPassword, userPassword);
}

export async function getByUsername(data, userName) {
  debug("@getByUsername");
  const sql = "SELECT * FROM user WHERE email=?";
  const result = await data.queryEntries(sql, [userName]);
  return result[0];
}
